package com.example.shredder.newlesson;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bMinus,bPlus;
    TextView tvCounter;
    int counter = 0;
    int i = 0,k=0;
    LinearLayout cChanges;

    int [] colors = {Color.RED,Color.GRAY,Color.GREEN,Color.BLACK,Color.BLUE};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bMinus = (Button) findViewById(R.id.b_minus);
        bPlus = (Button) findViewById(R.id.b_plus);
        tvCounter = (TextView) findViewById(R.id.tv_counter);
        cChanges = (LinearLayout) findViewById(R.id.color_this);
        bMinus.setOnClickListener(this);
        bPlus.setOnClickListener(this);
        //cChanges.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
                    case R.id.b_minus:
                        counter--;
                        if (i == 0) {
                            ++i;
                        }
                        --i;
                        if (i == colors.length) {
                            i = 0;
                        }
                        cChanges.setBackgroundColor(colors[i]);
                        break;

                    case R.id.b_plus:
                        counter++;
                        i++;
                        if (i == colors.length) {
                            i = 0;
                        }
                        cChanges.setBackgroundColor(colors[i]);
                        break;
                }
        tvCounter.setText(String.valueOf(counter));
    }
}
